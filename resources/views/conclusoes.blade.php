@extends ('templates.base')

@section('conteudo')
    <main>
        <h1>Conclusões</h1>
        <hr>
        <h3>Pilhas/Baterias</h3>
        <h5>Com os valores de resistência interna das pilhas e baterias podemos perceber quais delas estão carregadas ou descarregadas. Caso essa resistência seja alta, podemos concluir que a pilhas está ao fim de sua vida útil. Por outro lado, caso a resistência esteja baixa, indica-se que a pilha ainda está utilizável.</h5>
        <p></p>
        <p>A bateria Elgin é a bateria com maior resistência interna, ou seja, a que está mais descarregada, mesmo sendo recarregável sua resistência já é extremamente alta, isso quer dizer que sua vida útil já chegou ao fim.</p>
        <img src="../imgs/bateriaelgin.jpeg" alt="Bateria Elgin" height="200">
        <p></p>
        <p>A bateria Golite e a pilha Duracell Plus AAA também têm suas resistências internas altas, indicando que seus tempos de utilização já terminaram.</p>
        <img src="../imgs/golite.jpeg" alt="Bateria Golite" height="200"><img src="../imgs/duracellaaa.jpeg" alt="Pilha Duracell AAA" height="200">
        <p></p>
        <p>Já as pilhas que estão carregadas e que podem ser utilizadas ainda são aquelas as quais possuem baixa resistência interna, as pilhas medidas que estão mais próprias para uso são:</p>
        <img src="../imgs/duracellAA.jpeg" alt="Pilha Duracell AA">
        <p>Duracell AA</p>
        <img src="../imgs/freedom.jpeg" alt="Bateria Freedom" height="200">
        <p>Bateria Freedom</p>
        <hr>
        <h3>Conclusão Geral</h3>
        <h5>Com o trabalho conseguimos maximizar nossas habilidades e conhecimentos sobre as matérias Programação em Automação e Eletroeletrônica. Foi possível aplicar os conhecimentos teóricos sobre HTML, CSS e JavaScript fazendo o relatório. Além disso, montar a folha de estilos colocou nossa criatividade em exercício, o código HTML ajudou-nos a aplicar os nossos conhecimentos e a parte do JavaScript foi útil para podermos aprender um pouco mais sobre essa linguagem.
            Em relação à Eletroeletrônica conseguimos colocar em prática nossos conhecimentos por meio das medições e dos cálculos de resistência interna das pilhas e baterias.
        No geral, pensamos que esse trabalho tem alto valor acadêmico e que vai ajudar muito na nossa formação em técnicos de automação industrial.  </h5>
    </main>
    @endsection

@section('rodape')
<h10>Rodape das Conclusões </h10>
@endsection