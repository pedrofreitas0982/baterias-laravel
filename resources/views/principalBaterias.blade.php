@extends ('templates.base')

@section('conteudo')
    <main>
        <h1>Turma: 2D3 - Grupo 4</h1>
        <h2>Participantes:</h2>
        <table class="table table-dark table-striped table-bordered">
            <tr>
                <th>Matricula</th>
                <th>Nome</th>
                <th>Função</th>
            </tr>
            <tr>
                <td>0072556</td>
                <td>Pedro Augusto de Paiva Costa</td>
                <td>Gerente e Desenvolvedor HTML</td>
            </tr>
            <tr>
                <td>0072552</td>
                <td>Matias Dias Bezerra Oliveira Caetano</td>
                <td>Desenvolvedor CSS e JavaScript</td>
            </tr>
            <tr>
                <td>0072544</td>
                <td>Pedro Vitor da Silva Freitas</td>
                <td>Medições</td>
            </tr>
            <tr>
                <td>0066591</td>
                <td>Kemuel Victor dos Santos Quirino</td>
                <td>Medições</td>
            </tr>
        </table>
        <img src="imgs/grupo.jpeg" alt="Alunos do Grupo" height="240">
    </main>
@endsection

@section('rodape')
<h6>Rodape da pagina pricipal </h6>
@endsection