@extends ('templates.base')

@section('conteudo')
    <main>
        <h1>Teoria</h1>
        <hr>
        <h2>Resistência interna de uma fonte:</h2>
        <p> Uma fonte de tensão real é constituída de uma fonte de tensão ideal (E) em série com a sua resistência interna (r) como mostra o circuito abaixo:</p>
        <img src="../imgs/fonte-real.png" alt="Fonte Real">
        <p>Para conhecermos o valor da tensão interna da fonte (E) basta medirmos diretamente essa fonte com um multímetro desde que não haja nenhuma carga ligada a essa fonte:</p>
        <img src="../imgs/fonte-real (1).png" alt="Medição tensão interna">  
        <p>Quando conectamos uma carga (R) a essa fonte, circulará uma corrente (I) que produzirá queda de tensão interna (Vr) e a queda de tensão externa (VR):</p>
        <img src="../imgs/f2.png" alt="Conexão de carga">  
        <p>A equação para encontrarmos a equação interna, já simplificada é:</p>
        <img src="../imgs/Equacao.png" alt="Equação">
        <hr>
        <h2>O que são Pilhas/Baterias</h2>
        <p>Pilhas e baterias são dispositivos eletroquímicos usados para fornecer energia elétrica em aparelhos eletrônicos, dispositivos portáteis e uma ampla gama de equipamentos. Embora frequentemente sejam mencionados juntos, eles têm algumas diferenças importantes.</p>

            <h5>Pilhas:</h5> 
            <p>Uma pilha é um dispositivo eletroquímico simples, geralmente cilíndrico ou em formato de disco, composto por dois eletrodos (um positivo e um negativo) e um eletrólito que separa os eletrodos. O eletrólito é uma substância condutora de íons. Os eletrodos são feitos de materiais específicos, como zinco e manganês, que reagem quimicamente com o eletrólito para produzir energia elétrica.</p>
            
            <p>Quando um circuito elétrico é conectado aos terminais da pilha, ocorrem reações eletroquímicas nos eletrodos e no eletrólito, resultando em uma corrente elétrica que flui através do circuito. Conforme a reação química ocorre, os materiais nos eletrodos se esgotam, e a energia fornecida pela pilha diminui até que a pilha esteja completamente descarregada e pare de produzir energia. As pilhas primárias são projetadas para uso único e não são recarregáveis.</p> 
            
            <h5>Baterias:</h5>
            <p>Uma bateria, por outro lado, é composta por um ou mais elementos individuais (células) conectados em série ou paralelo para aumentar a tensão ou a capacidade da energia fornecida. As baterias podem ser feitas usando tecnologia semelhante à das pilhas, mas têm uma construção mais complexa e geralmente contêm várias células em um único invólucro. Elas podem ser recarregáveis, o que significa que, depois de esgotarem sua energia, podem ser recarregadas conectando-as a uma fonte de energia externa, revertendo as reações eletroquímicas e restaurando a energia da bateria.</p> 
            
            <p>As baterias recarregáveis são amplamente usadas em dispositivos eletrônicos, veículos elétricos, sistemas de armazenamento de energia e muito mais. Elas oferecem a vantagem de serem reutilizáveis, reduzindo o desperdício em comparação com as pilhas primárias, que são descartadas após o uso.</p> 
    </main>
    @endsection

@section('rodape')

<h6>Rodape da pagina Teoria</h6>

@endsection