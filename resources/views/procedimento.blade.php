@extends ('templates.base')

@section('conteudo')
    <main>
        <h1>Procedimento</h1>
        <hr>
        <h2>Medição:</h2>
        <p>1. Medição do valor do resistor utilizando o multímetro na escala de ohms (Ω);</p>
        <p>2. Medição da tensão real de todas as pilhas utilizando o multímetro na escala de tensão (V);</p>
        <p>3. Medição da tensão com carga das pilhas, ligando-as ao resistor e medindo com o multímetro na escala de tensão (V);</p>
        <p>4. Registramos no caderno os resultados obtidos, os quais foram utilizados para formar a tabela da página "Medições".</p>
        <p></p>
        <h2>Materiais utilizados:</h2>
        <h4>Multímetro:</h4>
        <img src="../imgs/multimetro.jpeg" alt="Multímetro" height="200">
        <p></p>
        <h4>Pilhas/Baterias:</h4>
        <img src="../imgs/1-destinacao-de-residuos-perigosos.jpg" alt="Pilhas/Baterias" height="200">
        <p></p>
        <h4>Resistor:</h4>
        <img src="../imgs/resistor.jpeg" alt="Resistor" height="200">
        <p></p>
        <p>*imagens ilustrativas</p>
    </main>
    @endsection

@section('rodape')
<h6>Rodape da pagina Procedimento </h6>
@endsection